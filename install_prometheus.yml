---
- name: Install Prometheus and Nginx
  hosts: prometheus
  become: yes
  tasks:
    - name: Update apt packages
      apt:
        update_cache: yes
        upgrade: yes

    - name: Install Nginx
      apt:
        name: nginx
        state: present

    - name: Create prometheus user and group
      group:
        name: prometheus
        system: yes

    - name: Create prometheus user
      user:
        name: prometheus
        system: yes
        shell: /sbin/nologin
        group: prometheus

    - name: Create prometheus data directory
      file:
        path: /var/lib/prometheus
        state: directory
        owner: prometheus
        group: prometheus

    - name: Download Prometheus archive
      get_url:
        url: https://github.com/prometheus/prometheus/releases/download/v2.53.2/prometheus-2.53.2.linux-amd64.tar.gz
        dest: /tmp/prometheus.tar.gz

    - name: Extract Prometheus archive
      unarchive:
        src: /tmp/prometheus.tar.gz
        dest: /opt
        remote_src: yes

    - name: Copy Prometheus binaries
      copy:
        src: "/opt/prometheus-2.53.2.linux-amd64/prometheus"
        dest: "/usr/local/bin/prometheus"
        mode: "0755"
        remote_src: yes

    - name: Copy promtool binary
      copy:
        src: "/opt/prometheus-2.53.2.linux-amd64/promtool"
        dest: "/usr/local/bin/promtool"
        mode: "0755"
        remote_src: yes

    - name: Create Prometheus configuration directory
      file:
        path: /etc/prometheus
        state: directory


    - name: Copy Prometheus configuration file
      copy:
        src: /opt/prometheus-2.53.2.linux-amd64/prometheus.yml
        dest: /etc/prometheus/prometheus.yml
        owner: prometheus
        group: prometheus
        remote_src: yes

    - name: Copy Prometheus web console files
      copy:
        src: "{{ item }}"
        dest: "/etc/prometheus/{{ item | basename }}"
        owner: prometheus
        group: prometheus
        mode: "0644"
      with_fileglob:
        - "/opt/prometheus-2.53.2.linux-amd64/consoles/*"
        - "/opt/prometheus-2.53.2.linux-amd64/console_libraries/*"


    - name: Copy Prometheus console libraries
      copy:
        src: "{{ item }}"
        dest: "/etc/prometheus/{{ item | basename }}"
        owner: prometheus
        group: prometheus
        mode: "0644"
      with_fileglob:
        - "/opt/prometheus-2.53.2.linux-amd64/console_libraries/*"


    - name: Set permissions for Prometheus binaries
      file:
        path: "/usr/local/bin/{{ item }}"
        owner: prometheus
        group: prometheus
        mode: "0755"
      with_items:
        - prometheus
        - promtool


    - name: Create prometheus systemd service file from template
      template:
        src: prometheus.service.j2
        dest: /etc/systemd/system/prometheus.service
      notify:
        - reload systemd


    - name: Configure Nginx for Prometheus
      template:
        src: nginx.conf.j2
        dest: /etc/nginx/sites-available/prometheus
      notify:
        - reload nginx
      vars:
        prometheus_server_ip: "127.0.0.1"

    - name: Enable site configuration for Prometheus
      file:
        src: /etc/nginx/sites-available/prometheus
        dest: /etc/nginx/sites-enabled/prometheus
        state: link
      notify:
        - reload nginx

    - name: Reload systemd units
      systemd:
        daemon_reload: yes

##################################

    - name: Read the content of prometheus.yml
      ansible.builtin.slurp:
        path: /etc/prometheus/prometheus.yml
      register: prometheus_config

    - name: Add new job to Prometheus configuration
      ansible.builtin.lineinfile:
        path: /etc/prometheus/prometheus.yml
        line: |
              - job_name: "Skillbox_service"
                static_configs:
                  - targets: ["54.237.155.213:9100"]
                    labels:
                      server_name: "My_app_server"
        insertafter: EOF
      when: "'Skillbox_service' not in prometheus_config.content | b64decode"


############################
    - name: Append configuration to prometheus.yml
      lineinfile:
        path: /etc/prometheus/prometheus.yml
        line: |
          - job_name: 'blackbox'
            metrics_path: /probe
            params:
              module: [http_2xx]
            static_configs:
              - targets:
                  - http://127.0.0.1:8080
            relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: 54.237.155.213:9115


    - name: Enable and start Prometheus service
      systemd:
        name: prometheus
        enabled: yes
        state: started

    - name: Reload Prometheus
      ansible.builtin.service:
        name: prometheus
        state: reloaded


  handlers:
    - name: reload nginx
      service:
        name: nginx
        state: reloaded

    - name: reload systemd
      systemd:
        daemon_reload: yes
