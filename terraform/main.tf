# Provider Block
provider "aws" {
  region = "us-east-1"
}

# Data sources
data "aws_availability_zones" "available" {}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Data source for VPC
data "aws_vpc" "selected" {
  tags = {
    Name = "my_vpc" # замените на имя вашей VPC
  }
}

# Security Group
resource "aws_security_group" "web" {
  name = "web-access"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9115
    to_port     = 9115
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "prometheus" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name               = "id_rsa"
  tags = {
    Name = "Prometheus"
  }
}

resource "aws_instance" "grafana" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name               = "id_rsa"
  tags = {
    Name = "Grafana"
  }
}


# EC2 Instance
resource "aws_instance" "my_appserver" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name               = "id_rsa"
  tags = {
    Name = "App Server"
  }

  # Добавляем атрибут, чтобы прикрепить экземпляр к target group
  lifecycle {
    create_before_destroy = true
  }

  # Создаем зависимость от ресурса целевой группы
  depends_on = [aws_lb_target_group.app_server_target_group]

  provisioner "local-exec" {
    command = "aws elbv2 register-targets --target-group-arn ${aws_lb_target_group.app_server_target_group.arn} --targets Id=${self.id}"
  }
}

# Elastic Load Balancer
resource "aws_elb" "web" {
  name               = "nginx-highly-available-elb"
  availability_zones = [for az in data.aws_availability_zones.available.names : az if az != ""]
  security_groups    = [aws_security_group.web.id]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:8080/"
    interval            = 70
  }

  tags = {
    Name = "Nginx-Highly-Available-ELB"
  }
}

# Target Group
resource "aws_lb_target_group" "app_server_target_group" {
  name     = "app-server-target-group"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.selected.id

  health_check {
    protocol = "HTTP"
    port     = 8080
    path     = "/"
  }
}

# Outputs
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}

