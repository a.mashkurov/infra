provider "aws" {
  region = "us-east-1"
}

resource "aws_route53_zone" "main" {
  name = "azamat-devops.uz"
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "www.azamat-devops.uz"
  type    = "A"
  ttl     = "300"
  records = ["86.62.2.147"]
}

resource "aws_route53_record" "cname" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "test.azamat-devops.uz"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx-highly-available-elb-770120626.us-east-1.elb.amazonaws.com"]
}

resource "aws_route53_record" "monitoring" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "monitoring.azamat-devops.uz"
  type    = "A"
  ttl     = "300"
  records = ["52.91.24.76"]
}

resource "aws_route53_record" "grafana" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "grafana.azamat-devops.uz"
  type    = "A"
  ttl     = "300"
  records = ["54.211.138.48"]
}

